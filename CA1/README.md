# Class Assignment 1 Report (CA1)
## Topic: Version Control with Git
## 1. Analysis, Design and Implementation

For this assignment, a previously created private Bitbucket repository was used.
It can be accessed at https://bitbucket.org/celsocastro/devops-21-22-atb-1211755/src/main/

Start date: 14th March
End date: 28th March

The "basic" folder of the Tutorial React.js and Spring Data REST application was used for this assignment.

This tutorial can be found here: https://github.com/spring-guides/tut-react-and-spring-data-rest

This assignment was separated in two main parts:

* Part 1: Implementation with no branches.
* Part 2: Implementation with branches.

##1.1. Part 1 - no branches

This part of the assignment was developed without the use of other branches except for the main branch.

To start this part of the assignment a copy of the Tutorial React.js and Spring Data REST Application was made into a new folder named CA1.

After this, we need to commit the changes, with the command:

```
git commit -a -m " CA1 Folder and copy code from Tutorial React.js and Spring Data REST Application."
```

And then push the changes to the remote:

```
git push origin main
```

Tags were used to mark the versions of the application, following the pattern major.minor.revision (e.g. v1.0.0).

This initial version was tagged as v1.1.0.

```
git tag v1.1.0
```

And then the tag was pushed to the remote:  

```
git push origin --tags
```

So our last commit will be tagged in the remote as v1.1.0.

![](ReadMeImages/GitReport.png)


After this, there was the need to develop a new feature to add a new field to the application.
A new field was added to record the years of the employee in the company, jobYears.

This required:

* Add support for the new field.
* Add unit tests for testing the creation of Employees and the validation
  of their attributes (for instance, no null/empty values). For the new field, only integer
  values should be allowed.
* Debug the server and client parts of the solution.

Changes were made in the application to fit these requirements. The DatabaseLoader class was changed to add this new 
field, too, as well as the app.js file. 

Several commits were made.

Then the following command was run in the command line to run the application and see the results in the browser, using http://localhost:8080/.

```
mvnw spring-boot:run
```

Here we can see the implementation of the Job Years field in the Class Employee and the subsequent commit to the repository 
with the tag v1.1.3.


``` java
 
 	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String jobTitle;
	private int jobYears;

	private Employee() {}

	public Employee(String firstName, String lastName, String description, String jobTitle, int jobYears) {

		validateString(firstName);
		this.firstName = firstName;

		validateString(lastName);
		this.lastName = lastName;

		validateString(description);
		this.description = description;

		validateString(jobTitle);
		this.jobTitle = jobTitle;

		validateInt(jobYears);
		this.jobYears = jobYears;
	}

```

 The validation of the Job Years field where the integer in question cannot be below zero.

```java
	
    private void validateInt(int intToValidate) {

		if (intToValidate < 0) {

			throw new IllegalArgumentException("Parameter " + intToValidate + " must be greater than Zero");
		}
	}
```

![](ReadMeImages/GitReport01.png)

 And here we have the unit test to validate the jobYears when creating a new Employee. This test throws an IllegalArgumentException when the jobYears is negative.

``` java

    @Test
    void shouldThrowExceptionNegativeJobYears() {

        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = -1;
        String email = "celsoCastroOmaior@gmail.com";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));
    }

```
After all this is done, we can se the full implementation visually on our localhost server. 

![](ReadMeImages/LocalHost01.png)

It's important to note that each time a new field is added to the original code we must reset the server in order for the changes to be visually active.

With the application running as intended, the final changes were committed and pushed to the remote repository.

This final version was tagged as v1.2.0, as well as ca1-part1, and pushed to remote repository.

```
git tag v1.2.0
git tag ca1-part1
```
```
git push origin --tags
```
And this is how it looks in the Bit Bucket Repository.

![](ReadMeImages/GitReport02.png)

This step concludes the core exercise.

##1.1. Part 2 - with branches

This part of the assignment was developed using other branches other than main.

A branch is used to develop new features for the application. In this case, there was a need to add a new email field
to the application 

In this part of the assignment, we need to add a new email field in the Class Employee without using the main branch from our repository.
Meaning that we need to create a secondary branch, add the new field and test its validation, go to the main branch and then merge with all the changes committed and pushed 
from the secondary branch, where in this case is named after the feature we are going to implement 

To create the new branch we use the following command:
```
git branch email-field
```

It's important to note that in git, when a new branch is created we don't automatically start working on it. 
We must switch the current working branch to the new one using the following command:
```
git checkout email-field
```

After this all commits are pushed to the email-field branch instead of master, as long as the branch is not changed in the commnad
line.

A new field was added for the employee's e-mail.

This required:

* Add support for the new field.
* Add unit tests for testing the creation of Employees and the validation
  of the new attribute (no null/empty values). 
* Debug the server and client parts of the solution.

Changes were made in the application to fit these requirements. The DatabaseLoader class was changed to add this new
field, too, as well as the app.js file.

Here we can see that code that was added to the Class Employee in order to meet the criteria mentioned
beforehand.

````java

@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String jobTitle;
	private int jobYears;
	private String email;


	private Employee() {}

	public Employee(String firstName, String lastName, String description, String jobTitle, int jobYears, String email) {

		validateString(firstName);
		this.firstName = firstName;

		validateString(lastName);
		this.lastName = lastName;

		validateString(description);
		this.description = description;

		validateString(jobTitle);
		this.jobTitle = jobTitle;

		validateInt(jobYears);
		this.jobYears = jobYears;

		validateString(email);
		this.email = email;
	}
````

With this in place we then processed to commit and push these changes before starting testing the validation

````
git commit -a -m "Add validation to email field in Class Employee"

git push origin email-field 
````

Now it's a good time to mention that was we were updating the implementation of the task at hand, we were adding different tags 
with each iteration of the application. In this case this was version 1.2.2.

Also in the image below we can see the blue line (email-field branch) with a dot next to the red line (main branch). This means that a push was
made in the other branch. 

![img.png](ReadMeImages/img.png)


After these brief changes when then processed to add the Unit Testing necessary for the email field validation.

```` java

    @Test
    void shouldThrowExceptionBlankEmail() {

        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 1;
        String email = "";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));
    }

    @Test
    void shouldThrowExceptionNullEmail() {

        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 1;
        String email = null;

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));
    }

    @Test
    void shouldThrowExceptionInvalidEmail() {
        
        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 1;
        String email = "celsocastroOmaiorgmail.com";

        // Act + Assert

        Employee employeeOne = new Employee (firstName, lastname, description, jobTitle, jobYears, email));

    }
````

In these Unit Tests we check if the String email is either null or empty, meaning that a valid email
field for the employee can only be created when none of these conditions are met, throwing an exception
when they are in fact met. 

Then the following command was run in the command line to run the application and see the results in the browser, using http://localhost:8080/.

```
mvnw spring-boot:run
```

In the images below we can see the implemented change in the application:

![](ReadMeImages/localHost02.PNG)


After debugging, the final changes were committed and pushed to the remote repository, as done previously, to the
created email-field branch:

```
git commit -a -m "Add Testing to email Validation in Class Employee."
```

```
git push origin email-field
```

![img_1.png](ReadMeImages/img_1.png)

With all set in stone in this branch, we now need to switch over to the main branch in order to merge all this together.

To switch branches we used the following command:

```
git checkout main
```

Then, with the following command, the email-field branch was merged into the master branch.
```
git merge email-field
```

After this we committed and pushed all the changes to the repository and marked with the tags "v1.3.0" and "ca1-part2"


Now the remote only has one branch once again.

![](ReadMeImages/GitReport04.PNG)

After this, there was the need to create a new branch to fix the new email field, as the created validations
only covered cases when the field was empty or null. A new branch was created, fix-invalid-email, to add new validations
for the email format (e.g., an email must have the @ sign).

While these type of validations in the email field (Not being null nor blank) are perfectly reasonable they don't, however
validate if the email string is the correct format, which in this case is the verification of a @ in the String.

In order to fix this issue, which is considered a bug fix, we need to create a secondary branch once again but this time 
with the name of the bug fix.

So the branch was created with the following name with the git branch command:

```
git branch fix-invalid-email
```
We then switch to that branch in order to work on this bug fix. 

```
git checkout fix-invalid-email
```
To add the new validations and fix the bug, it was required to:

* Add a new method using a regular expression that validates the correct email format.
* Add new tests to validate that only a correctly formatted email String is accepted.
* Debug the server and client parts of the solution.

In the code snippet below we can see that the validation method for the email changed from validateString to validateEmail.

````java

@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String jobTitle;
	private int jobYears;
	private String email;


	private Employee() {}

	public Employee(String firstName, String lastName, String description, String jobTitle, int jobYears, String email) {

		validateString(firstName);
		this.firstName = firstName;

		validateString(lastName);
		this.lastName = lastName;

		validateString(description);
		this.description = description;

		validateString(jobTitle);
		this.jobTitle = jobTitle;

		validateInt(jobYears);
		this.jobYears = jobYears;

		validateEmail(email);
		this.email = email;
	}

````

And here we can the algorithm that checks for the @ Character in the String.

````java

	private void validateEmail(String email) {

		String regexPattern = "^(.+)@(\\S+)$";

		if (Objects.isNull( email ) || email.isBlank()) {

			throw new IllegalArgumentException("Email cant be null or blank");
		}

		 if (Pattern.compile(regexPattern)
				.matcher(email)
				.matches()) {

			 return;
		 }

		 throw new IllegalArgumentException("Email must contain @");
	}
````

And next we see the tests made for the email field, where if the String doesn't contain it throws an exception.

````java

    @Test
    void shouldThrowExceptionInvalidEmail() {
        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 1;
        String email = "celsocastroOmaiorgmail.com";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));

    }
````

After these steps, the changes were committed and pushed to the fix-invalid-email branch:
```
git push origin fix-invalid-email
```

Here we can see that pushed commit in our repository
 
![img_2.png](ReadMeImages/img_2.png)

With the validations fully implemented and tested it's now time to switch over to the main branch for merging.

```` java
git checkout main
````

Then the fix-invalid-email branch was merged into the master branch.
```
git merge fix-invalid-email
```

This concluded the exercise, so the repository was tagged with the tag ca1-part2:
```
git tag ca1-part2
```

Finally, we have here the final push of this exercise in our bitbucket repository.

![img_3.png](ReadMeImages/img_3.png)

## 2. Analysis of an Alternative

An alternative to Git is Mercurial (Hg). Both Mercurial and Git are free DVCS - distributed version control systems.

Both allow developers to bring the repository code to the personal machines,
perform their work items, and then put it back into a central server.


## 2.1 Main differences

### 2.1.1 Syntax

While Git and Mercurial work similarly, Git is more complex than Mercurial. Mercurial’s syntax is simpler, so
it can be easier to adopt.

### 2.1.2 Security

By default, Mercurial does not allow the user to change the history. Mercurial allows the user to rollback to last pull
or commit and change it, but not further back.

In comparison, Git allows all involved developers to change the version history.

This means Git is more flexible, but can become hard to secure the codebase with new/inexperienced developers. This
leads to the conclusion that Mercurial is safer for new developers, while Git can be better for experienced developers.

### 2.1.3 Branching

Git’s branching model is more effective. In Git, branches are only references to a certain commit. Git allows the user
to create, delete, and change a branch anytime, without affecting the commits. 

In Mercurial, branches refer to a linear line of consecutive changesets.
Changesets refer to a complete set of changes made to a file in a repository.

Mercurial embeds the branches in the commits, where they are stored.
This means that branches cannot be removed because that would alter the history.

It requires the user to take added care not to push code to the wrong branch.


### 2.1.4 Staging area

Git supports the idea of a staging area, which is also known as the index file.

Staging is the practice of adding files for the next commit. 
It allows the user to choose which files to commit next.
This is useful when you don't want to commit all changed files together.

In Mercurial, there is no index or staging area before the commit. Changes are committed as they are in the
working directory. If a similar option is required in Mercurial, the user needs to use extensions.

## 3. Implementation of the Alternative

##Part 1 - no branches

To implement the alternative, first the Mercurial software (6.1) was installed in the local machine.

There was also the need to choose a remote repository that works well with Mercurial. For
that, HelixTeamHub was used. The remote link is:

https://helixteamhub.cloud/gorgeous-stone-2020/projects/devops/repositories/devops/tree/default

After that, a new copy of the basic tutorial folder was added to a new folder called CA1 to start the project.

In the command line in the correct path to that folder, the following command was used to add all untracked files 
to the Mercurial remote:
```
hg add
```
Then, a commit was made with those changes:
```
hg commit -m "Add spring-rest-tutorial do Mercurial Repository"
```
This initial version was tagged with v1.1.0:

```
hg tag v1.1.0
```

And the changes were pushed to the HelixTeamHub remote:

```
hg push
```

It's important to note here that everytime we push to the Mercurial Repository we must always input our Repository Username and password.

After this the necessary changes were made to the application to meet the requirements for the first part of the exercise:

* Add support for the new field jobYears.
* Add unit tests for testing the creation of Employees and the validation
  of their attributes (for instance, no null/empty values). For the new field, only integer
  values should be allowed.
* Debug the server and client parts of the solution.

There is no need to show what changes were made in the code since they are already mentioned above.

After the changes and debugging a new commit was made and pushed to the remote, before being tagged as v1.2.0 and
ca1-part1.
```
hg commit -m "Add Unit Testing to Class Employee"
```

The same tags used in git are used here also.

```
hg tag v1.2.0
hg tag ca1-part1
```
```
hg push
```

In this part of the exercise the differences between git and mercurial are very minimal, the only difference here being the lack of necessity of pushing tags to the repository.
These tags are related to the latest commit and are pushed with the code.

This concluded the first part of the exercise.

##Part 2 - with branches

When it comes with the implementation of the second part of the exercise with the alternative,
this is where we start seeing a few big differences between Mercurial and Git.

The code implementation is, once again, the same as mentioned before so there is no need to refer it
here again. 

I will, however, explain the differences between Mercurial and Git when it comes to branch creation and usage.

The creation of a branch is the same as in Git, with the following command:

````
hg branch email-field
````

But, as you can see in the image below, when a branch is created in mercurial it isn't added to the
local Repository. But when you create a branch you are automatically switched over to it and ready to work.

![img_4.png](ReadMeImages/img_4.png)

Only when u commit the changes made in the new branch is where the branch is added to the local Repository as you can see in the image below


![img_5.png](ReadMeImages/img_5.png)

That is only major difference between these two version control softwares.

After all the changes are done in the new branch it's now time to switch over to 
main branch once again with the following command.

````
hg update default
````

Note: In Mercurial the main branch is called default and instead of the checkout command we have update. 

In order to merge all the changes made we use the following command:

````
hg merge email-field
````

If we check the branches with the command:

````
hg branches
````

We can see that we switched over to default successfully but what's interesting when changing to the default branch
is that the secondary branch becomes inactive as we can see in the image below

![img_6.png](ReadMeImages/img_6.png)

This becomes helpful when dealing with several branches as it tells you which ones are being used and which ones aren't.

After all of this we then processed to do the same fix-email-field branch as previously mentioned.

We use the following command in order to create the branch.

````
hg branch fix-email-field
````

Then after the changes are made we commit the branch 

````
hg commit fix-email-field
````

And then it's added to the repository.

![img_7.png](ReadMeImages/img_7.png)

Once again we change to the default branch (main)

````
hg update default
````

We then proceed to merge the changes made in the other branch

````
hg merge fix-invalid-email
````

And, as expected the fix-invalid-email branch becomes inactive after the merge

![img_8.png](ReadMeImages/img_8.png)

And with this we conclude the Alternative for the second part of the exercise.




