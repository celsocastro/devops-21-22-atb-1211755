# Class Assignment 3 Report (CA3)
## Topic: Virtualization with Vagrant

* ###Start date: 19th April

* ###End date: 9th May

* ### Development Repository : https://bitbucket.org/celsocastro/devops-21-22-atb-1211755/src/main/

## 1. Analysis, Design and Implementation

In this week assignment we were introduced to the concept of Virtualization, namely, what is it, why do we use it, how it works and why it's an important tool in a developers arsenal. 

To start, what is virtualization exactly?

Simply put, Virtualization is a technique that allows a computer to run other virtual instances 
of a computer system in a layer abstracted from the actual hardware. These emulated equivalents of computers systems are run on top
of another system and these are called virtual machines. These virtual machines are implemented by a hypervisor. 

A hypervisor is a program for creating and running virtual machines, for example Virtual Box or VmWare.

Essentially, there are two types of virtualization:

* Hardware-lever virtualization: Where the Hypervisor runs directly on the host hardware.
* Hosted virtualization: Hypervisor runs on the host OS.

In our case we are dealing with Hosted Virtualization where we just open a Hypervisor and run the Virtualization we need.

There are many advantages to using Virtualization like for example:

1. Test our application in different environments (Linux, OS and Windows for example).
2. Create Solutions to run on Cloud.
3. Keeping the development and deployment environments in sync.

This assignment was separated in two main parts:

* Part 1: Practice with VirtualBox.
* Part 2: Use Vagrant to setup a virtual environment.

For The First Part:

- For the first week of the assignment we are to set up our own VirtualBox to create a Virtual Machine with Ubuntu, so we can run previous projects
from former assignments.


- The ultimate goal here is to mainly see how a Virtual Box works, how it's setup based on our own hardware and to see how to we interact
between the host machine and virtual machine.


For The Second Part:
- For the second week of the assignment we now use Vagrant to understand how it works to help us to set up one or more Virtual Machines and why it's a popular option 
when it comes the usage of Vm's.

For context, Vagrant is a software used to create and maintain one or more Virtual Machines, using VirtualBox, VMware, Docker containers, etc.

What makes Vagrant such a powerful tool is that it greatly simplifies the management of VMs by:

- Helping us automate the setup of one or more VMs.
- It allows us to define all the configuration of a Vm using a simple configuration file.
- Can run several operating systems and different providers(i.e. Hypervisors)
- Sharing a VM becomes sharing a vagrant configuration file.
 
##1.1. Part 1 - Practice with VirtualBox.

Part 1 is actually quite simple since all we need to do is create our own Virtual Machine, copy our won repository inside the Vm and run our former projects and that's it.

We start by downloading a Hypervisor which in this case we are going to use Virtual Box.

We downloaded through this link : https://www.virtualbox.org/wiki/Downloads 

In this particular case we are using Virtual Box for windows but there are options for OS, Linux and even Solaris if there is need.

Now open up the Virtual Box software and this should appear (Minus the Virtual Machines Already created).

![img.png](Images/img.png)

We can now start creating and configure our very own virtual machine.

To being just press the button that says "New" and this should open up

![img_1.png](Images/img_1.png)

Here you can choose the name of the virtual machine, where it's going to be located inside your machine and what operating system and version of it 
we want to emulate in our host computer.

Since in this particular case we want to emulate Ubuntu 

![img_2.png](Images/img_2.png)

Now we need to allocate some RAM to the virtual machine. This is limited to the amount of
memory that the host machine has available. Obviously you can't use up all the memory, so it's recommended
to use between 1024 and 2048 mb (Depending on what purpose the virtual machine has).

![img_3.png](Images/img_3.png)

In our case we will use 2048 mb.

![img_4.png](Images/img_4.png)

After that, we wish to add a virtual hard disk to the virtual machine.

Here just leave the default option where we create a virtual disk now.

![img_5.png](Images/img_5.png)

Since we are using an Ubuntu image we select the option VDI.

![img_6.png](Images/img_6.png)

Here we choose for the storage on the physical hard disk to be dynamically allocated. Meaning that
our virtual machine isn't going to use all the space at once, but instead it fills up as we use it.

![img_7.png](Images/img_7.png)

Since these are going to be regular computer files we need to select our Virtual Hard Disk location and 
choose the size of the amount of file data it can have. 

By default, it's marked at 10.00 GB, and we are going to leave it has it is.

![img_8.png](Images/img_8.png)

Now that we successfully created our very own Virtual Machine with the main components ready to go. it's now time to configure it, so we can use it.

Right now our Virtual Machine is empty. It doesn't have any operating system inside it. How do we resolve this?

We want to install Ubuntu, so we need an Ubuntu Image that we are going to get throughout this link :

https://help.ubuntu.com/community/Installation/MinimalCD

After you downloaded and saved the Ubuntu Image, go back to Virtual Box, select the newly created Virtual Machine 
and click on settings.

![img_12.png](Images/img_12.png)

Here you can see all the different settings we can change for our Virtual Computer but right we need a operating system 
so click on storage.

![img_13.png](Images/img_13.png)

Now just click on the Empty just under Controller: IDE and after that click on the blue cd icon. 

![img_14.png](Images/img_14.png)

Now just select the iso file with Ubuntu that you just downloaded.

![img_15.png](Images/img_15.png)

And don't forget to check the Live CD/DVD box since this is basically telling the machine that this is a bootup Cd. 

![img_16.png](Images/img_16.png)

After this is done you can start running your virtual machine to install Ubuntu inside the emulation
  
Concluded the installation, don't forget to remove the ubuntu image from the settings or else everytime we run the machine
it's going to install ubuntu.

Now you should turn on the virtual machine and this should appear.

![img_17.png](Images/img_17.png)


Still it's not all done. We now need to head over to setting again and select Network where we can 
a NAT adapter already there. This one is needed since it allows the virtual machine to connect to the Internet.

But now we need to create a second Network Adapter of the Type Host-Only Adapter, so we can connect between the host and 
virtual machine.

![img_19.png](Images/img_19.png)

Here we can see that we already have a Host-Only Adapter created automatically, so we are going to use this one.

![img_20.png](Images/img_20.png)


![img_21.png](Images/img_21.png)


![img_22.png](Images/img_22.png)


Now start the VM, log on into it and let's continue the configuration.

First you need to update the packages repositories with the following command:

* sudo apt update

Now install the network tools like so:

* sudo apt install net-tools

We need to edit the network configuration file to setpup the IP, and to do that we do:

* sudo nano /etc/netplan/01-netcfg.yaml

This should open up the text editor and you need to make sure the file contents are as follows:

````
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
      dhcp4: yes
    enp0s8:
      addresses:
        - 192.168.56.5/24


````

Save and exit the editor and now just apply the new changes :

* sudo netplan apply

Install openssh-server so that we can use ssh (Secure Shell) to open secure terminal sessions to the VM (from other hosts)

* sudo apt install openssh-server

Enable password authentication for shh:

* sudo nano /etc/ssh/sshd_config
* uncomment the line PasswordAuthentication yes
* sudo service ssh restart

Install an ftp server so that we can use the FTP protocol to transfers files to/from
the VM (from other hosts):

* sudo apt install vsftpd

Enable write success for vsftpd:

* sudo nano /etc/vsftpd.conf
* uncomment the line write_enable = YES
* sudo service vsftpd restart

Quite a few configuration to do, but we can finally use the virtual machine however we aren't going to
use it directly. Instead, we are going to use the host machine to work in the VM. 

To do this, just open your host machine Terminal and type in the following command based on how 
you customized the machine.

* "Ubuntu Username"@"Ip defined in the netplan files"

In my case it's something like this :

* celso@192.168.56.5

![img_23.png](Images/img_23.png)

Done correctly, and we should be able to control the virtual machine through the host terminal. 

![img_24.png](Images/img_24.png)

Since the FTP server is enabled in the VM we can now use a ftp application, like
FileZilla, to transfer files to/from the VM.

* To download FileZilla we used this link: https://filezilla-project.org

After downloading and opening up the application we should have something very similar to this.

![img_25.png](Images/img_25.png)

If we look closely in the upper part of the application we can se a Host, Username and password boxes. 
There input boxes allows us to connect to the virtual machine like so.

![img_26.png](Images/img_26.png)

Connecting to the virtual machine you can see on the left the host machine files and on the right the virtual
machine files. To transfer files all we need to do is drag and drop from the left, what file we want and drop it 
where we want it to the right.

![img_27.png](Images/img_27.png)

Now we need to install Git and Java by using the following commands:

* sudo apt install git

* sudo apt install openjdk-11-jdk-headless

With Git and Java installed we are now ready to install and execute the Spring Tutorial application.

Note that every command that we are doing from now on it's on the host terminal connected to the virtual machine.

With this being said we now are going to make a clone of the application using the following command:

* git clone https://github.com/spring-guides/tut-react-and-spring-data-rest.git

Now change to the directory of the basic version like so:

* cd tut-react-and-spring-data-rest/basic

Now just build and execute the application:

* ./mvnw spring-boot:run

Now to open the application in our host computer, use the browser and use the correct url,
which in our case is: http://192.168.56.5:8080/

Note that previously instead of an IP we used localhost. This is because before we would do everything in the host machine and the localhost
is the standard host name provided to the address of the local computer.

Since we are not using the local computer now and instead a virtual machine we use the Ip of the virtual machine.

Done everything correctly and should have something like this.

![img_28.png](Images/img_28.png)


Now let's do this to all the previously made assignments.

Head back to the root directory of the virtual machine and copy our private repository:

* git clone https://CelsoCastro@bitbucket.org/celsocastro/devops-21-22-atb-1211755.git

Where we have CA1 Assignment being run from the host terminal using the virtual machine. 
As we can see it's very similar to the application we just executed. 

![img_29.png](Images/img_29.png)

Now where is where it gets a bit different. 

If we recall, the assigment CA2 Part1 we used chatroom application where we needed to run the server and then run the client.

Since we used the host machine we could do both from the host terminal without any issue.

But now we can't run both server and client in the virtual machine. Why is that?

Since the Ubuntu image we are using doesn't have any User Interface if we try running the client it doesn't work.

But we can use the virtual machine to host the server and use our host machine to run the Client.

We do need to change a few things for this to work.

Originally our runClient task connected to the localHost but as previously mentioned we aren't using local host anymore.

![img_30.png](Images/img_30.png)

So all you need to change is the localHost args to the virtual machine IP.

![img_31.png](Images/img_31.png)

Now you can run the server in the virtual machine and then use the host terminal (without connecting to the VM)
to run the Client Task.

Done correctly we should have something like this.

![img_32.png](Images/img_32.png)

Now the rest of the task are pretty straightforward. But we are going to need FileZilla to check if the task are working correctly

As we can see the gradle_basic_demo only has 3 files, and we want to run the copyTask to copy the src file. 

![img_33.png](Images/img_33.png)

Running the copyTask we can see that we successfully created a backup file with the src files inside.

![img_34.png](Images/img_34.png)

The same result happens when we run the zip Task, and it creates a new folder called backupZip.

![img_35.png](Images/img_35.png)

Going in to the second part of the CA2 assignment with the included alternative we can see the same results happening. In this case being the dist files
being created after running the task from the terminal connected to the virtual machine.

![img_36.png](Images/img_36.png)

## Part 1.2 : Use Vagrant to setup a Virtual Environment.

In the Second Part of the assignment we use Vagrant as means to configure our Virtual Box. 

So start this assignment we are going to use and analise the VagrantFile located in : https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/
as our initial solution.

In this Vagrant File we can see that it creates two virtual machines.

First we start by saying what is the box we are going to use which in this case is ubuntu-xenial.

After that we have some provisions that are going to be common between both Vm's that we are going to install

![](Images/img_40.png)

Here starts the configuration of the first virtual machine that contains the H2 Database.

It has the same box as previously mention.

![](Images/img_41.png)

We also need to give it an Ip address and map the ports.

![](Images/img_42.png)

Then, we want to install another provision, this one being the H2 Database jar file.

![](Images/img_43.png)

In the bottom we have an irregular provision that we always want to execute. This is because,
in this specific case, we need to put in effect the H2 database jar file with java, that this Virtual Machine
previously downloaded.

![](Images/img_44.png)

And here we have the configuration for the webserver Virtual Machine that is going to host Java and React.

The same box that was mentioned before is algo going to be used here.

The IP Address is also necessary to add, and it's going to belong on the same network as the database VM.

We also set 1GB of memory for this machine.

![](Images/img_45.png)

Lastly here are the provisions that we are going to need to be able to run the react-and-spring-
data-rest-basic tutorial.

And in the bottom it's how we are going to run our application.

We clone our application from our private repository. 

Go to the folder that contains the gradle files

The chmod is to give us execute permissions on the gradlew file

And then we build the app

Now our application is going to generate a war file that then gets copied to a folder inside tomcat8 (where the application is going to run).

![](Images/img_46.png)

With all of this setup we can now head on to the terminal and run the following command.

* vagrant up

This will take some time to download and setup everything, but it really doesn't compare
to the amount of time that we would spend manually creating the virtual box and installing the 
necessary applications.

When it's done, it should look like this in the terminal to let us know that the build was successful.

Now use the command `vagrant status` to check the status of all our Virtual Machines. In this case it should 
only show up two Vm's, the web and db like so. 

![img.png](Images/img_53.png)


In the host you can open the spring web application using one of the following options:

    http://localhost:8080/basic-0.0.1-SNAPSHOT/
    http://192.168.54.10:8080/basic-0.0.1-SNAPSHOT/

And if all is working correctly we should get this result :

![](Images/img_49.png)

You can also open the H2 console using one of the following urls:

    http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
    http://192.168.54.10:8080/basic-0.0.1-SNAPSHOT/h2-console

And for the JBDC URL connection use : 

    jdbc:h2:tcp://192.168.54.11:9092/./jpadb

![](Images/img_47.png)

If all is working correctly we should be able to the Database as follows.

![](Images/img_50.png)

We can also feed the Database with more data like so:

![](Images/img_51.png)

And that data will be updated in the db Virtual machine and fetched by the web VM.

![](Images/img_52.png)

And this concludes this week assignment.

# 2. Analysis of the Alternative - VMware

The alternative to VirtualBox that we will be using here is VMware, more specifically
VMware Workstation Pro.

We say more specifically because there are several versions of the software, depending on what
context we need the virtualization for.

Workstation Pro only has a 30-day trial period, so if we want to use it for more than that we need to purchase the license
but this version contains a full set of virtualization features.

There is a free version called VMware Player for personal, educational and non-commercial use, however
it doesn't contain the full set of features.

This becomes VirtualBox biggest advantage since that hypervisor is free for any use desired.

| Comparison                | VirtualBox | VMware |
| -----------               | ----------- | ----------|
| Software Virtualization      | Yes       |     No      |
| Hardware Virtualization   | Yes        |     Yes      |
| Host Operating Systems      | Linux, Windows, Solaris, macOS, FreeBSD      |    Linux, Windows + macOS (requires VMware Fusion)       |
| Guest Operating Systems      | Linux, Windows, Solaris, macOS, FreeBSD       |    Linux, Windows, Solaris, FreeBSD + macOS (with VMware Fusion)       |
| User Interface       | Graphical User Interface (GLI) and Command Line Interface (CLI)       |    Graphical User Interface (GLI) and Command Line Interface (CLI)       |
| Snapshots   | Yes        |    Snapshots only supported on paid virtualization products, not on VMware Workstation Player       |
| Virtual Disk Format      | VDI, VMDK, VHD, HDD       |    VMDK       |
| Virtual Disk Allocation Type      | Preallocated: fixed disks;   Dynamically allocated: dynamically allocated disks;      |    Preallocated: provisioned disks;   Dynamically allocated: thin provisioned disks;       |
| Virtual Network Models      | Not attached, NAT, NAT Network, Bridged adapter, Internal network, Host-only adapter, Generic (UDP, VDE)       |   NAT, Bridged, Host-only + Virtual network editor (on VMware workstation and Fusion Pro)        |
| USB Devices Support   | USB 2.0/3.0 support requires the Extension Pack (free)       |   Out of the box USB device support        |
| 3D Graphics      | Up to OpenGL 3.0 and Direct3D 9;   Max of 128 MB of video memory; 3D acceleration enabled manually    |  Up to OpenGL 3.3, DirectX 10;   Max of 2GB of video memory; 3D acceleration enabled by default         |
| Integrations      | VMDK, Microsoft’s VHD, HDD, QED, Vagrant, Docker      |  Requires additional conversion utility for more VM types;   VMware VSphere and Cloud Air (on VMware Workstation)         |
| VirtualBox Guest Additions vs. VMware Tools      | Installed with the VBoxGuestAdditions.iso file       |   Install with a .iso file used for the given VM (linux.iso, windows.iso, etc.)        |
| API for Developers   | API and SDK      |  Different APIs and SDKs         |
| Cost and Licenses     | Free, under the GNU General Public License       |     VMware Workstation Player is free, while other VMware products require a paid license      |

Source : https://phoenixnap.com/kb/virtualbox-vs-vmware

Now with that out the way, let's see how Vmware differs from VirtualBox in the Vagrant File.

Now that we have Two hypervisors installed we need to specify in the vagrant file which one we want to use, since vagrant uses
VirtualBox by default.

The `config.vm.provider` argument is how is tell vagrant what Virtualization Software we want to use.

In this case we want to use VMware Workstation Pro, so therefore we write `config.vm.provider "vmware_workstation"` like so.

![](Images/img_54.png)

But if we try to run this vagrant file with the provided VMware it won't work because although the Vagrant Application
has all the necessary configurations to run Virtualbox by default, it doesn't contain to run VMware.

This being said, we need to install two things :

1. https://www.vagrantup.com/vmware/downloads 
    * The Vagrant  vmware Utility


2. `vagrant plugin install vagrant-vmware-desktop`
    * The vagrant vmware provider plugin
    * This installation is on the terminal where the file is located

Now just write `vagrant up --provision` in the terminal and wait a few minutes for configuration.

If all went well we should have the Virtual Machines in the Vmware software.

![](Images/img_55.png)

We can open the spring boot application using one of these options.

    http://localhost:8080/basic-0.0.1-SNAPSHOT/
    http://web/basic-0.0.1-SNAPSHOT/

The web VM should be working just like it did with the VirtualBox.

![](Images/img_56.png)

We can open the Database with this option.

    http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console

And JDBC URL is:

      jdbc:h2:tcp://192.168.57.11:9092/./jpadb

Again, it should work just like it did previously.

![](Images/img_57.png)

And as we can see it all works just fine how it should.

![](Images/img_58.png)

We can also insert data to the Database.

![](Images/img_59.png)

And that data will be updated in the frontend.

![](Images/img_60.png)

And with that we conclude the alternative of this week assigment.
