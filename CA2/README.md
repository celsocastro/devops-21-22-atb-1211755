# Class Assignment 2 Report (CA2)
## Topic: Build Tools with gradle
## 1. Analysis, Design and Implementation

For this assignment, a previously created private Bitbucket repository was used.
It can be accessed at https://bitbucket.org/celsocastro/devops-21-22-atb-1211755/src/main/

Start date: 4th April
End date: 11th April

In this assigment we start using and analysing how build tools work in java when it comes to the automation of the code compilation, linking and packaging and
transforming it in executable form.

The two builds that we will be looking at are the following:

1. Gradle
2. And as the alternative, Maven

This assignment was separated in two main parts:

* Part 1: Practice gradle with an application demo
* Part 2: Convert the basic version of the Tutorial application to Gradle (instead of Maven)

For The First Part:

- For the first week of the assigment we used and example application available at https://bitbucket.org/luisnogueira/gradle_basic_demo/.


- The ultimate goal is to have a sort of "playground" to experiment with the tools that
  gradle has to offer.


For The Second Part:
- For this assignment we created a completely new gradle spring boot project except the src folder which eventually
  is replaced with the original spring boot tutorial made in the First Class Assigment (CA1).


- The website used to create this new gradle project was: https://start.spring.io/


- Here we learn how to convert the basic version (i.e., "basic" folder) of the Tutorial to Gradle and experiment how to all works
  with a blank gradle file and set it up to integrate frontend

##1.1. Part 1 - Practice gradle with an application demo

This part of the assignment was developed in a similar fashion as CA1 with the several commits expected, branch use and issues in BitBucket.

To begin, start by downloading and commit to your repository (in a folder for Part1 of CA2) the example application available at https://bitbucket.org/luisnogueira/gradle_basic_demo/. We will be
working with this example.

Now that the project is in a folder we can start experimenting with gradle.

We can start by building the gradle project with the following command:

````
./gradlew build
````

This task is used to assemble and test our application. In our case since we are in a Java Project it applies
automatically the Java Plugin.

After the build is done and successful we can check what tasks are available in our project.

For that we use the following command;

````
./gradlew tasks
````

This should have the following output in the terminal :

````
------------------------------------------------------------
Tasks runnable from root project 'react-and-spring-data-basic'
------------------------------------------------------------

Application tasks
-----------------
bootRun - Runs this project as a Spring Boot application.

Build tasks
-----------
assemble - Assembles the outputs of this project.
bootBuildImage - Builds an OCI image of the application using the output of the bootJar task
bootJar - Assembles an executable jar archive containing the main classes and their dependencies.
bootJarMainClassName - Resolves the name of the application's main class for the bootJar task.
bootRunMainClassName - Resolves the name of the application's main class for the bootRun task.
build - Assembles and tests this project.
buildDependents - Assembles and tests this project and all projects that depend on it.
buildNeeded - Assembles and tests this project and all projects it depends on.
classes - Assembles main classes.
clean - Deletes the build directory.
jar - Assembles a jar archive containing the main classes.
testClasses - Assembles test classes.

Build Setup tasks
-----------------
init - Initializes a new Gradle build.
wrapper - Generates Gradle wrapper files.

Documentation tasks
-------------------
javadoc - Generates Javadoc API documentation for the main source code.

Help tasks
----------
buildEnvironment - Displays all buildscript dependencies declared in root project 'react-and-spring-data-basic'.
dependencies - Displays all dependencies declared in root project 'react-and-spring-data-basic'.
dependencyInsight - Displays the insight into a specific dependency in root project 'react-and-spring-data-basic'.
dependencyManagement - Displays the dependency management declared in root project 'react-and-spring-data-basic'.
help - Displays a help message.
javaToolchains - Displays the detected java toolchains.
outgoingVariants - Displays the outgoing variants of root project 'react-and-spring-data-basic'.
projects - Displays the sub-projects of root project 'react-and-spring-data-basic'.
properties - Displays the properties of root project 'react-and-spring-data-basic'.
tasks - Displays the tasks runnable from root project 'react-and-spring-data-basic'.

Verification tasks
------------------
check - Runs all checks.
test - Runs the test suite.

Rules
-----
Pattern: clean<TaskName>: Cleans the output files of a task.
Pattern: build<ConfigurationName>: Assembles the artifacts of a configuration.
````

Essentially this shows us what base tasks are available to us.

Now that we experimented with gradle we commit and push to our repository,

We now need to add a task to be able to execute our server.

For this, we start by opening our build.gradle file located in the root of the project.

When you open the build.gradle file you should have a task called runClient that open our application.

That task should be the following :

````
task runClient(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on localhost:59001 "
  
    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatClientApp'

    args 'localhost', '59001'
}
````

What's important here to take note are the two variables in the end, mainClass and args.

The mainClass indicates which java class is executed when we run the task.

The args show us to what server are going to connect to.

Currently, we don't have any server, so we need to run one with a gradle task.

For this we made the following task:

```
task runServer(type:JavaExec, dependsOn: classes){
group = "DevOps"
description = "Launches a chat client that connects to a server on localhost:59001 "

    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatServerApp'

    args '59001'
}
````
Here we set the mainClass to point to the Java Class that runs the server side of the application and with the args we choose the domain name which is needed to connect to our server.

After this is done we switch to the terminal to check is it's all working as intended.

To start we obviously need to run our server, so we now go to the command line and type in what gradle task to run, which in our
case is the following:

````
./gradlew runServer
````

This will start running the chat room server.

Since one terminal is running the server, and we want to test our application and for that we
open a new terminal and run the following command:

````
./gradlew runClient
````

This should open a chatbox where you chat with yourself in this case and since all seems to be working properly
we can proceed with the rest of the assignment and commit and push to our repository to save our changes.

We are now tasked to create a task of the type Copy where we create a backup folder of the sources of the application
which in this case we want to copy the contents of the src folder.

This task is quite simple to build and not as complex as the one before.

You should build this task:

````
task copyTask(type: Copy) {
    from 'src'
    into 'backup'
}
````

All that is needed is select the type of task we want in this way we just need to write Copy, and it does just that.
And all that is needed is the file that it's going to copy from and where it's going to put it.

Here we select the file we want to copy with the from keyword where all we need to insert the name of the file, which in this case is src.

Then with the into keyword, we choose where to put the copied files. We choose a file named backup since it seems apropriate and since there is no file called backup
the task will create one for us.

Then just run the task in the command line as follows:

```
./gradlew copyTask
```

And it should create a backup folder in the root where inside resides the contents of the src file.

Once again, after all this is done we commit and push to our repository.

For our final task, we now want to create a task of the type Zip to make an archive (i.e., zip file) of the sources of the application.

For this we build the following task:

````
task zip(type: Zip) {
    from 'src'
    into 'backup.zip'
    destinationDir(file("backupZip"))
}
````

Very similar to the one before but here the task type becomes Zip, and we also use the destinationDir keyword is which is were we want
our zipped file to go to.

Now just run this task in the command line as follows:

````
./gradlew zip
````

This should create a BackupZip folder with the zipped source folder inside.

To end this assignment we add the tag ca2-part1 and commit and push to our repository and that concludes our Assignment for this first week.

Side Note: This first week assignment has no alternative variation.

##1.2. Convert the basic version of the Tutorial application to Gradle (instead of Maven)

The goal of this week is to convert the basic version (i.e., "basic" folder ) of the Tutorial
application of Gradle (instead of Maven).

This way we can see how a newly created gradle project works and what implications there are when it comes to integrating
it with the frontend part of our application.

To start all of this we first must create a new branch named "tut-basic-gradle". We are going to use this branch for this part of the assigment.

You should do something like this :

````
git branch tut-basic-gradle

git checkout tut-basic-gradle
````

Now that we are in the correct working branch we now need to create our gradle project.

For this, we use https://start.spring.io/ to start our gradle spring boot project from scratch with the following dependencies:
Rest Repositories, ThymeLeaf, JPA, H2.

Extract the generated zip file inside the folder "CA2/Part2/" of your repository. We
now have an "empty" spring application that can be built using gradle.

Now since we have an "empty" application, and we need want to convert our tutorial app that we used in CA1 assignment we need to delete our src folder
and replace it with the same one from CA1 Assignment.

To do this we first deleted the src folder with the following command:

```
rmdir C:\Switch2021\Segundo_Semestre\DevOps\Devops01\devops-21-22-atb-1211755\CA2\part2\react-and-spring-data-rest-basic\src
```

Just put in rmdir and specify the relative path to the file we wish to delete.

Now we want to copy the src file from the basic of the tutorial.

````
Xcopy C:\Switch2021\Segundo_Semestre\DevOps\Devops01\devops-21-22-atb-1211755\CA1\basic\src C:\Switch2021\Segundo_Semestre\DevOps\Devops01\devops-21-22-atb-1211755\CA2\part2\react-and-spring-data-rest-basic /e
````

We also need copy the webpack.config.js and package.json

````
Xcopy C:\Switch2021\Segundo_Semestre\DevOps\Devops01\devops-21-22-atb-1211755\CA1\basic\src\webpack.config.js C:\Switch2021\Segundo_Semestre\DevOps\Devops01\devops-21-22-atb-1211755\CA2\part2\react-and-spring-data-rest-basic /e

Xcopy C:\Switch2021\Segundo_Semestre\DevOps\Devops01\devops-21-22-atb-1211755\CA1\basic\src\package.json C:\Switch2021\Segundo_Semestre\DevOps\Devops01\devops-21-22-atb-1211755\CA2\part2\react-and-spring-data-rest-basic /e
````

Now all the correct files should be added to our project from the tutorial.

We can now experiment with the application by using:

````
./gradlew bootRun
````

Notice that the web page http://localhost:8080 is empty! Since we build this project from scratch we don't have the necessary plugins needed for gradle to deal with the frontend code.
So that's what we are going to do next step by step.

Step One:
- We will add the gradle plugin org.siouan.frontend to the project so that gradle is
  also able to manage the frontend.

  This plugin can be found here : https://github.com/Siouan/frontend-gradle-plugin


We need to add one of the following lines to the plugins block in build.gradle (select the line
according to your version of java: 8 or 11):

````
id "org.siouan.frontend-jdk8" version "6.0.0"
id "org.siouan.frontend-jdk11" version "6.0.0"
````

In our example we add the jdk11 version to the plugins block in build.gradle.

Step Two:
- Now we need to configure the previous plugin by adding the following code in the build.gradle file.

````
frontend {
nodeVersion = "14.17.3"
assembleScript = "run build"
cleanScript = "run clean"
checkScript = "run check"
}
````

Step Three:
- We now need to update the scripts section/object in package.json to configure the execution of the webpack:

````
"scripts": {
"webpack": "webpack",
"build": "npm run webpack",
"check": "echo Checking frontend",
"clean": "echo Cleaning frontend",
"lint": "echo Linting frontend",
"test": "echo Testing frontend"
},
````

Following these steps correctly, and you should have everything ready to run our application, but first we need to run the following command:

````
./gradlew build
````

Since there is new code inside the gradle.build file we need to run this command so that the tasks related to the frontend are also executed and the frontend code is generated.

After this is done we once again run the bootRun command like so:

````
./gradlew bootRun
````

And you now should have a frontend page that isn´t empty and contains the excepted frontend that we have been working on these past few weeks.

Now let's add a task to gradle to copy the generated jar file to a folder named "dist" located at the project root folder level.

For this open up the build.gradle file and put in the following code:

````
task copyJar(type: Copy) {
	from 'build/libs'
	into 'dist'
}
````

This task configuration is in very similar fashion to the Copy Task we made in the first part of this assigment.

When u run this task with the ./gradlew copyJar command it should generate a dist folder with a copy of the generated jar file inside.

Now, to finish this week's assignment, let's build a gradle task to delete all the files generated by webpack (usually located at
src/resources/main/static/built/). This new task should be executed automatically by gradle before the task clean.

First things first, we create the task that deletes the files generated by the webpack, so we add this code snippet to the build file:

````
task deleleBuiltFiles(type: Delete) {
	delete "scr/main/resources/static/built"
	println "Cleaning Files..."
}
````

Once again it's very similar to what we have been doing so far what changes here is the task type (which in this case it's Delete) and the relative path to
the file we want to delete.

The println is not necessary, all it does it just prints out whatever line we want it to print when running this task. In this case it just show "Cleaning Files..."

Now, this task should be executed automatically by gradle before the task clean

This means that we are never supposed to run this task directly, but instead we run the clean task that should be able to depend on the task we buildt.

To configure this setting all you need to do is add this to the build.gradle file:

````
clean.configure {
	dependsOn deleleBuiltFiles
}
````

All this does it configure the clean task to be dependent on the deleteBuiltFiles tasks so this way everytime we run the clean task it first runs our custom task.

Now when you run the clean task in the command line it should remove the built files made by the webpack.

Now just commit the code, change to the main branch and merge all the changes needed

After all this is done we create the tag ca2-part2 and commit and push our changes to the repository and thus ends this week's assigment.

## 2. Analysis of an Alternative

An alternative to Gradle is Apache Maven. Both Gradle and Apache Maven are free to use Build tools for developers for the building of any application.

Both are very different when

When it comes to the final functionality of both tools they are practically similar, after all they are used to compile code into executable form so nothing special here.

Now how both tools reach that end goal and what customization options are available differ drastically from Gradle to Maven and that's what we are going to analise.

## 2.1 Main differences

### 2.1.1 Flexibility

Google chose Gradle as the official build tool for Android; not because build scripts are code, but because Gradle is modeled in a way that is extensible in the most fundamental ways. Gradle's model also allows it to be used for native development with C/C++ and can be expanded to cover any ecosystem. For example, Gradle is designed with embedding in mind using its Tooling API.

Both Gradle and Maven provide convention over configuration. However, Maven provides a very rigid model that makes customization tedious and sometimes impossible. While this can make it easier to understand any given Maven build, as long as you don’t have any special requirements, it also makes it unsuitable for many automation problems. Gradle, on the other hand, is built with an empowered and responsible user in mind.

### 2.1.2 Performance

Improving build time is one of the most direct ways to ship faster. Both Gradle and Maven employ some form of parallel project building and parallel dependency resolution. The biggest differences are Gradle's mechanisms for work avoidance and incrementality. The top 3 features that make Gradle much faster than Maven are:

    Incrementality — Gradle avoids work by tracking input and output of tasks and only running what is necessary, and only processing files that changed when possible.
    
    Build Cache — Reuses the build outputs of any other Gradle build with the same inputs, including between machines.

    Gradle Daemon — A long-lived process that keeps build information "hot" in memory.


### 2.1.3 Dependency Management

Both build systems provide built-in capability to resolve dependencies from configurable repositories. Both are able to cache dependencies locally and download them in parallel.

As a library consumer, Maven allows one to override a dependency, but only by version. Gradle provides customizable dependency selection and substitution rules that can be declared once and handle unwanted dependencies project-wide. This substitution mechanism enables Gradle to build multiple source projects together to create composite builds.

Maven has few, built-in dependency scopes, which forces awkward module architectures in common scenarios like using test fixtures or code generation. There is no separation between unit and integration tests, for example. Gradle allows custom dependency scopes, which provides better-modeled and faster builds.

Maven dependency conflict resolution works with a shortest path, which is impacted by declaration ordering. Gradle does full conflict resolution, selecting the highest version of a dependency found in the graph. In addition, with Gradle you can declare versions as strictly which allows them to take precedence over transitive versions, allowing to downgrade a dependency.


## 3. Implementation of the Alternative

##Part 2 - Convert the basic version of the Tutorial application to Maven (instead of Gradle)

For our alternative we are going to convert the Tutorial into Maven, although the original project is already in Maven
, here we seek out to learn how to properly set up a Maven project from scratch in order for the application to be able to deal with
the frontend code.

After this, we'll see the main differences between building the same tasks we build in gradle onto Maven.

We'll be doing the same steps as previously done in part 2 of the assigment up until the frontend configuration since that's were both build tools differ.

Instead of manipulating a build.gradle file we will be using the maven equivalent which is the pom.xml file.

This is where all the plugins, dependencies and general Maven configurations are located so whenever we want to change something about our build configuration this is where we go.

For starters add the following plugin to our pom.xml file :

````
	<plugin>
	  <groupId>com.github.eirslett</groupId>
	  <artifactId>frontend-maven-plugin</artifactId>
	  <version>1.12.1</version>
	</plugin>

	<plugin>
	  <groupId>org.apache.maven.plugins</groupId>
	  <artifactId>maven-compiler-plugin</artifactId>
	  <configuration>
		<source>11</source>
		<target>11</target>
	  </configuration>
	</plugin>
````

This, essentially is the Maven version of the same plugin we added in Gradle for it to be able to run.

And the plugin after that is the one that configures the one above so that all can work properly.

With this you should have a working application with the frontend part integrated.

To run the application, just type the following command on the terminal:

````
./mvnw spring-boot:run
````

Now we are on to the task building, and they are the same ones as before.

- A Task to copy the generated jar file into a file named "dist" located at the project root folder level


- A Task to delete all the files generated by webpack that is executed automatically.

Although we have been using the name tasks, those are only applied to gradle. Here in Apache Maven we add dependencies
and configure them to our liking and goal.

Here is an example of a copy dependency for the generated files .

````
	<plugin>
		<artifactId>maven-resources-plugin</artifactId>
		<version>3.0.2</version>
		<executions>
			<execution>
				<id>copy-resource-one</id>
				<phase>generate-sources</phase>
				<goals>
					<goal>copy-resources</goal>
				</goals>
				<configuration>
					<outputDirectory>dist</outputDirectory>
					<resources>
						<resource>
							<directory>target</directory>
							<includes>
								<include>*.jar</include>
							</includes>
						</resource>
					</resources>
				</configuration>
			</execution>
		</executions>
	</plugin>
````

The most important difference to take note here is that, unlike Gradle, here we don´t create a task that later we can run in the terminal.

Maven is based around the central concept of a build lifecycle. What this means is that the process for building and distributing a particular artifact (project) is clearly defined.

For the person building a project, this means that it is only necessary to learn a small set of commands to build any Maven project, and the POM will ensure they get the results they desired.

There are three built-in build lifecycles: default, clean and site. The default lifecycle handles your project deployment, the clean lifecycle handles project cleaning, while the site lifecycle handles the creation of your project's web site.

Each of these build lifecycles is defined by a different list of build phases, wherein a build phase represents a stage in the lifecycle.

Validate, compile, test, package, verify, install and deploy are all sequentially the lifecycle phases of a default lifecycle.

So, in order to be able to run our Copy Plugin we need to tell Maven which phase he is going to execute the Copy.

In this case is in the generate-sources phase. of the build lifecycle.

That is the main difference and the biggest set back compared to gradle. This limitation can really hinder your personal customization making it excruciating compared to gradle
where you have much more control and freedom.

After this plugin is build we are ready to see if it works.

You should use the following command to start the installation phase of the default life-cycle to see if it's all running correctly:

````
./mvnw install
````

After this you should have a dist folder that contains a copy of the jar file inside in the project source root.

Now onto the other dependency that we need to set up.

Same principle as before where the deletion of the built files is before the clean so you should have something like this in your pom.xml

````
  <plugin>
	 <artifactId>maven-clean-plugin</artifactId>
	 <version>2.5</version>
		<executions>
			<execution>
				<id>auto-clean</id>
				<phase>initialize</phase>
				<goals>
					<goal>clean</goal>
				</goals>
				<configuration>
					<filesets>
						<fileset>
							<directory>
								src/main/resources/static/built
							</directory>
						</fileset>
					</filesets>
				</configuration>
			</execution>
		</executions>
  </plugin>
````

Now when you run ./mvnw clean in your terminal it should delete all the built files located inside the src folder.

And with that we conclude the use of the alternative for this week's assignment.

Some closing thoughts in the use of the alternative:

- Gradle showed to be much superior build tool than Maven in several occasions. Not only is it faster and more efficient than Apache Maven could ever be
  it's also highly customizable since it's built upon what Maven helped define.


- You can definitely see the old age of Maven show up when compared side by side with Gradle. The syntax, the life-cycle phases, etc., all show to be a little out dated
  and with the limitation when it comes to the customization can be a big drawback for some developers, especially with no ability to create tasks of our own.


- Nevertheless, Maven is still impressive in its own right, being the more simple and concrete build tool for simple Projects where you just want something to compile your code.

All in all, both are extremely reliable build tools for developers and both serve their own niche in the community when it comes to each functionality these are able to provide.


