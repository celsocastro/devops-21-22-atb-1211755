/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String jobTitle;
	private int jobYears;
	private String email;


	private Employee() {}

	public Employee(String firstName, String lastName, String description, String jobTitle, int jobYears, String email) {

		validateString(firstName);
		this.firstName = firstName;

		validateString(lastName);
		this.lastName = lastName;

		validateString(description);
		this.description = description;

		validateString(jobTitle);
		this.jobTitle = jobTitle;

		validateInt(jobYears);
		this.jobYears = jobYears;

		validateEmail(email);
		this.email = email;
	}

	private void validateString(String stringToValidate) {

		if (Objects.isNull( stringToValidate ) || stringToValidate.isBlank()) {

			throw new IllegalArgumentException("Parameter " + stringToValidate + " Cannot be Null or Empty");
		}
	}

	private void validateInt(int intToValidate) {

		if (intToValidate < 0) {

			throw new IllegalArgumentException("Parameter " + intToValidate + " must be greater than Zero");
		}
	}

	private void validateEmail(String email) {

		String regexPattern = "^(.+)@(\\S+)$";

		if (Objects.isNull( email ) || email.isBlank()) {

			throw new IllegalArgumentException("Email cant be null or blank");
		}

		 if (Pattern.compile(regexPattern)
				.matcher(email)
				.matches()) {

			 return;
		 }

		 throw new IllegalArgumentException("Email must contain @");
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(id, employee.id) &&
			Objects.equals(firstName, employee.firstName) &&
			Objects.equals(lastName, employee.lastName) &&
			Objects.equals(description, employee.description) &&
			Objects.equals(jobTitle, employee.jobTitle)	&&
			Objects.equals(jobYears, employee.jobYears) &&
			Objects.equals(email, employee.email);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id, firstName, lastName, description, jobTitle, jobYears, email);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getJobTitle() { return jobTitle;}

	public void setJobTitle(String jobTitle) {this.jobTitle = jobTitle;}

	public String getEmail() {return email;}

	public void setEmail(String email) {this.email = email;}

	public int getJobYears() {
		return this.jobYears;
	}

	public void setJobYears(int jobYears) {
		this.jobYears = jobYears;
	}

	@Override
	public String toString() {
		return "Employee{" +
			"id=" + id +
			", firstName='" + firstName + '\'' +
			", lastName='" + lastName + '\'' +
			", description='" + description + '\'' +
			", jobTitle='" + jobTitle + '\'' +
				", jobYears=" + jobYears + '\'' +
				", email=" + email + '\'' +
				'}';
	}
}
// end::code[]
