package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;



class EmployeeTest {

    @Test
    void shouldCreateValidEmployeeJobTitleGreaterThanZero() {

        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 2;
        String email = "celsoCastroOmaior@gmail.com";

        // Act + Assert
        Employee employeeOne = new Employee(firstName, lastname, description, jobTitle, jobYears, email);
    }

    @Test
    void shouldCreateValidEmployeeJobTitleEqualsZero() {

        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 0;
        String email = "celsoCastroOmaior@gmail.com";

        // Act + Assert
        Employee employeeOne = new Employee(firstName, lastname, description, jobTitle, jobYears, email);
    }

    @Test
    void shouldThrowExceptionBlankFirstName() {

        // Arrange

        String firstName = "";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 2;
        String email = "celsoCastroOmaior@gmail.com";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));

    }

    @Test
    void shouldThrowExceptionNullFirstName() {

        // Arrange

        String firstName = null;
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 2;
        String email = "celsoCastroOmaior@gmail.com";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));

    }

    @Test
    void shouldThrowExceptionBlankLastName() {

        // Arrange

        String firstName = "Celso";
        String lastname = "";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 2;
        String email = "celsoCastroOmaior@gmail.com";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));
    }

    @Test
    void shouldThrowExceptionNullLastName() {

        // Arrange

        String firstName = "Celso";
        String lastname = null;
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 2;
        String email = "celsoCastroOmaior@gmail.com";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));
    }

    @Test
    void shouldThrowExceptionBlankDescription() {

        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "";
        String jobTitle = "Junior Developer";
        int jobYears = 2;
        String email = "celsoCastroOmaior@gmail.com";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));
    }

    @Test
    void shouldThrowExceptionNullDescription() {

        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = null;
        String jobTitle = "Junior Developer";
        int jobYears = 2;
        String email = "celsoCastroOmaior@gmail.com";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));
    }

    @Test
    void shouldThrowExceptionBlankJobTitle() {

        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "";
        int jobYears = 2;
        String email = "celsoCastroOmaior@gmail.com";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));
    }

    @Test
    void shouldThrowExceptionNullJobTitle() {

        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = null;
        int jobYears = 2;
        String email = "celsoCastroOmaior@gmail.com";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));
    }

    @Test
    void shouldThrowExceptionNegativeJobYears() {

        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = -1;
        String email = "celsoCastroOmaior@gmail.com";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));
    }

    @Test
    void shouldThrowExceptionBlankEmail() {

        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 1;
        String email = "";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));
    }

    @Test
    void shouldThrowExceptionNullEmail() {

        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 1;
        String email = null;

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));
    }

    @Test
    void shouldThrowExceptionInvalidEmail() {
        // Arrange

        String firstName = "Celso";
        String lastname = "Castro";
        String description = "Gajo mesmo fixe";
        String jobTitle = "Junior Developer";
        int jobYears = 1;
        String email = "celsocastroOmaiorgmail.com";

        // Act

        assertThrows(IllegalArgumentException.class, () ->
                new Employee (firstName, lastname, description, jobTitle, jobYears, email));

    }

}