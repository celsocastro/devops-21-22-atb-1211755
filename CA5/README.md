# Class Assignment 5 Report (CA5)
## Topic: CI/CD Pipelines with Jenkins

* ###Start date: 23rd May

* ###End date: 6 June

* ### Development Repository : https://bitbucket.org/celsocastro/devops-21-22-atb-1211755/src/main/

## 1. Analysis, Design and Implementation

##1.1. Part 1 - CI/CD Pipelines with Jenkins - Practice with Jenkins.

The first part of the assignment involves creating a very simple pipeline with Jenkins.

Start by downloading Jenkins from the following url : https://www.jenkins.io/download/

You will have several download options depending on what machine you have. For this Class Assignment we will download 
the Generic Java Package (.war).

Once you downloaded Jenkins, open up the terminal, go to the folder where you have the Jenkins.war file and do the
following command, so you can finish the installation.

``java -jar jenkins.war``

Now open https://localhost:8080 to access Jenkins and execute the necessary additional setup step.

If all was setup correctly you should end up with something very similar to this ( Minus the builds).

![img.png](CA5_Images/part1/img.png)

Now let's begin the exercise at hand. To Start, click on the _**New Item**_ button in the top left corner of Jenkins.
This button will take you to the following page where we define what name we want our item to have and what type it's
going to be, in this case we want to select the **_Pipeline_** Option and then click _**Ok.**_

![img_1.png](CA5_Images/part1/img_1.png)

Since we choose _**Pipeline**_ we now want to define what that pipeline is going to actually do. But before we get into
that it0s important to note that there are two different ways of building the pipeline.

First, we can build the Script file when we select the _**Pipeline script**_ option like so:

![img_2.png](CA5_Images/part1/img_2.png)

Or we can build the Script file ( called Jenkinsfile ) in the root folder of the source code repository.

![img_3.png](CA5_Images/part1/img_3.png)

Both methods yield the same results, and we are going to show both ways of completing the task.

It is also important to note the Script for either method is written in the language groovy(the same used by gradle) , 
a alternative to the Java language that borrows some elementos from Python, Ruby and Smalltalk.

### Pipeline Script Method :

We want to define the following stages in our pipeline: 

  - **Checkout**. To checkout the code form the repository.
  - **Assemble**. Compiles and Produces the archive files with the application. We will use the
              _**assemble**_ task of gradle, so it doesn't run the tests. 
  - **Test**. Executes the Unit Tests and publish in Jenkins the Test results. See the junit
    step for further information on how to archive/publish test results.
  - **Archive**. Archives in Jenkins the archive files (generated during Assemble).

Following these guidelines for the pipeline you should end up with something very similar to this.

![](CA5_Images/part1/Jenkins_pipeline.png)

The syntax used for this Pipeline is the Declarative syntax

- The `pipeline` argument is how we start a _Declarative Pipeline_ and inside the curly braces of the
arguments is where all the instructions of the pipeline is going to do.


- The ``Stages`` argument is where we are going to define what each individual `stage` is going to do.


- The `Stage` argument is where we define what the stage is and what it's going to do.
  - It's here we tell what the Script file is going to do, for example any gradle task is defined in this spectrum, so 
essentially any terminal command.
  - There is also the isUnix() method which checks if the machine that's running the Script file is either windows or 
Linux and runs the commands accordingly.


- In the Test `stage` we run the junit command and store the test results in a .xml file inside the generated build folder.


- In the Archiving `stage` we run the archiveArtifacts command to store the build artifacts
(for example, distribution zip files or jar files) so that they can be downloaded later. 

And that's all we need to define for our pipeline so now just press apply.  

Now in the main page of our pipeline press the _**Build now**_ button to see if the script is running correctly.

![img_4.png](CA5_Images/part1/img_4.png)

If all is working accordingly you should get something similar to this:

![img_5.png](CA5_Images/part1/img_5.png)

And this concludes this method of building the script file, now lets check how to do the otherway.

### Pipeline Script from SCM Method :

  We want to execute the exact same pipeline we previously built but this time we are building the Jenkinsfile (Script file)
inside the root folder of our source code.

  Just open up the root folder of the ca2/part1 application and create a new file and call it Jenkinsfile

  Now just copy the entire script that we previously built and paste it inside the newly created Jenkinsfile.

  You should have something similar to this:

![](CA5_Images/part1/VScode_jenkinsfile.png)

Just push these changes into the repository, so it can be used by Jenkins later.

Now we want to build a new pipeline and in the definition of the **Pipeline** select the **Pipeline
script from SCM** option and do the final configurations.

You should have something similar to this:

![](CA5_Images/part1/Jenkins_Pipeline_SCM.png)


  - We define what repository we want to use



  - What branch it's using 



  - Define what the path is to the Jenkinsfile. 

After this is done, press again the **Build Now** button and if all is working correctly you should get the exact same results
as the other method.

![](CA5_Images/part1/JenkinsFile_Build_success.png)

And with this we conclude the first week assignment of CA5.


## Part 1.2 : CI/CD Pipelines with Jenkins - Build the tutorial spring boot application.

 For this week's assignment we were tasked with creating a slightly more complex Pipeline that involves
a few extras steps compared to last week's task. The main addition compared to last week's pipeline is that
now, inside the Script file e are going to create a Docker image and publish it in Docker Hub.

  The source code that we will be using is the code from CA2_part2 ( which in this case i copied the files into a folder
  inside CA5 to make sure there is no conflict with previous assignment).


 For this task I'll be defining the Script file inside Jenkins and not creating the Jenkinsfile inside the
root folder of the application.

 Before we start the pipeline Script, since we were tasked with building and deploying a docker image, we need to create
the dockerfile inside the root folder of our source code. 

Here's our very simple dockerfile that all it does is deploy the generated .war file into tomcat.

````FROM tomcat:9.0.63

## build and deploy
COPY ./build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

## expose port to outside as public
EXPOSE 8080

````

And here we have the Jenkinsfile script.

````pipeline {
agent any    
environment {
registry = "celsocastro/jenkins_part2"
registryCredential = "docker_id"
dockerImage = ""
}

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git branch: 'main', url: 'https://bitbucket.org/celsocastro/devops-21-22-atb-1211755'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir ("CA5/part2/react-and-spring-data-rest-basic") {
                    script {
                        if (isUnix())
                            sh '''
                            chmod +x gradlew
                            ./gradlew clean assemble
                            '''
                        else
                            bat './gradlew assemble'
                    }
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                dir ("CA5/part2/react-and-spring-data-rest-basic") {
                    script {
                        if (isUnix()){
                            sh './gradlew test'
                            junit 'build/test-results/test/*.xml'
                        }else{
                            bat './gradlew test'
                            junit 'build/test-results/test/*.xml'
                        }
                    }
                }
            }
        }
        stage('Javadoc') {
            steps {
                echo 'Generating javadoc...'
                dir ("CA5/part2/react-and-spring-data-rest-basic") {
                    script {
                        javadoc javadocDir: 'build/reports/tests/test', keepAll: false
                    }
                }
            }
        }
        stage('HTML') {
            steps {
                echo 'documenting...'
                dir ("CA5/part2/react-and-spring-data-rest-basic"){
                    publishHTML([
                        allowMissing: false, 
                        alwaysLinkToLastBuild: false, 
                        keepAll: false, 
                        reportDir: 'build/reports/tests/test', 
                        reportFiles: 'index.html', 
                        reportName: 'HTML Report', 
                        reportTitles: 'Docs Loadtest Dashboard'
                        ])
                }
            }
        }
        stage('Archive') {
            steps {
                echo 'Archiving...'
                dir ("CA5/part2/react-and-spring-data-rest-basic"){
                    archiveArtifacts 'build/libs/*.war'
                }
            }
        }
        stage('Docker Image') {
            steps {
                echo 'Creating docker image...'
                script {
                    dir ("CA5/part2/react-and-spring-data-rest-basic"){
                        dockerImage = docker.build registry + ":${env.BUILD_ID}"
                    }
                }
            }
        }
        stage('Docker Deploy') { 
            steps { 
                echo 'Deploying docker image...'
                script { 
                    docker.withRegistry( '', registryCredential ) { 
                        dockerImage.push() 
                    }
                } 
            }
        }
    }
} 
````

 It's essentially the same as the previous pipeline expect were adding 4 extras stages and a few more configurations
 declared as arguments inside the `environment` keyword.

  Before the `stages` we have :

  - `registry`: This variable will contain the docker image name that we wish to add.


  - `registryCredential` : This refers the credentials we created inside Jenkins in order to be able to push our 
Docker Image to Docker Hub.


 In the `stages` we have :

  - `dockerImage` : Declared empty but will be reassigned a value later in the pipeline when building the image. 



  - The Javadoc `stage` is to generate the proper documentation of our source code.


- The HTML `stage` grabs the built Javadoc in the previous stage and generates a Html page with all the info. 



- The Docker image `stage` builds the docker image with the dockerfile we added to the root folder of the source Code.

  - Here we add a name with the registry value that was previously mentioned and the generated ID when building the docker image. 



- The Docker Deploy `stage` is where we push the previously built image into Docker Hub using registry credentials that we 
created in Jenkins.

  
As you can see this pipeline doesn't differ a lot from the previous built pipeline except for the steps when building 
a docker image which are a little more complex than we thought.

Pressing the `Build Now` button and if all is working correctly we should get something similar to this :

![img_6.png](CA5_Images/part1/img_6.png)

And here is the generated Javadoc :

![img_7.png](CA5_Images/part1/img_7.png)

And the generated HTML with the Javadoc :

![img_8.png](CA5_Images/part1/img_8.png)

But more importantly, here is the docker image we built the pipeline pushed to the docker hub.

![img_9.png](CA5_Images/part1/img_9.png)


And with that we conclude this week assignment.

# 2. Analysis of the Alternative
