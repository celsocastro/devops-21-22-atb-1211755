# Class Assignment 4 Report (CA4)
## Topic: Containers with Docker

* ###Start date: 9th May

* ###End date: 23rd May

* ### Development Repository : https://bitbucket.org/celsocastro/devops-21-22-atb-1211755/src/main/

## 1. Analysis, Design and Implementation

##1.1. Part 1 - Containers with docker - Practice with Docker.

 The first part of the assignment involves creating docker images and running containers using the chat application from
previous assignment CA2.

In essence, we want to create a docker image with the chat application server, run a container with said image and finally, use
the main host computer to connect to the Chat Server Application.

This is very similar to what we did previously on CA3 with VirtualBox and Vagrant.

There will be Two versions of the exercise:

- We build the chat server "inside" the Dockerfile.

- We build the chat server in our host computer and copy the jar file "into" the Dockerfile.

To start the first version of the exercise we need to set up a few things first.

   - Go to https://www.docker.com/ and install Docker Desktop if you run MacIS or Windows.
   - In case you run Linux install Docker Engine that can be found here https://hub.docker.com/search?q=&type=edition&offering=community&operating_system=linux

// Relembrar para referir o resto da instalação.

After Docker is installed we can now start the simple exercise at hand.

To begin, open up your IDE of choice (in our case it's going to be Visual Studio Code) and create a file called Dockerfile like so.

![img.png](img.png)

Opening up the Dockerfile reveals that it's empty, and we can finally start working.

We mentioned before that this is very similar to the way we configure a Vagrant File but thankfully, this is more simple 
and straight forward.

To be able to run the chat server inside the container, your Dockerfile should look similar to this.

![](CA4 Images/VScode_dockerCode.png)

Here we can see several docker instructions used ( The words in purple ) so we are going to explain what each one does.

- `FROM` : Initializes a new build stage and sets the Base Image for subsequent instructions. A valid `Dockerfile` must
start with a `FROM` instruction. In this case we want an ubuntu image with version 18.04.

- `RUN` : Will execute any commands in a new layer on top of the current image and commit the results. the resulting committed image
will be used for the next step in the `Dockerfile`. Here we want update the packages, install Java 8, install git and install
unzip. All tools needed for the exercise.

- `WORKDIR` : Sets the working directory for any instructions that follow it in the `Dockerfile`. Here we are telling the `Dockerfile`
that the working directory will be `/root`.

Then it's the same as the Vagrant File, just with Docker instructions now.

We execute `RUN` to clone our repository.

Use `WORKDIR` to go inside the application folder.

Use `RUN` to change the permissions of the gradlew file and then build the Jar file of the application.

- `EXPOSE` : Informs Docker that the container listens on the specified network ports at runtime. Here we want
to our container to listen to the 59001 network port.


- `CMD` : The main purpose of this instruction is to provide defaults for an executing container. Meaning that, in this case, when we
execute the container, by default it's going to run the Jar file that we built before listening to the 59001 network port.
There can only be one `CMD` in a `Dockerfile`.

With the Dockerfile fully set up we can now build the container, and to do that go to the terminal and write the following command.

``docker build -t [image name] .
``

it's important to note that each folder can only have one `Dockerfile` and that this command must be run inside that folder
or else docker won't know what `Dockerfile` to use to build the container.

In `[image name]` you can put whatever name you'd like.

Running this command should yield this results

1. Here we can see that we successfully created a Docker image with the name chat_server_image.

![](CA4 Images/VScode_dockerBuild.png)

Now let's build the container with our image with the following command in the terminal:

``Docker run -d -p 59001:59001 [docker image name]
``

`-d` : This starts the container in detached mode

`-p` : Is used to explicitly map a single port or range of ports. In this case we Map the port 59001 of the host to port 59001 in the container. 

Like so:

![](CA4 Images/Vscode_dockerRun.png)

If all is correct you should have a working Docker container running the Chat App Server.

Going to Docker desktop, and you can see a container has been created and is running.

Running the previous command creates a random generated container name with our image.

![](CA4 Images/DockerDesktop_Containers.png)

You can also see the image that we created being used by the container.

![](CA4 Images/DockerDesktop_Image.png)

Proceeding to inside the container, and you can see indeed confirm that the chat server is running, so all we need
to do now is to check if we can actually connect to it.

![](CA4 Images/DockerDesktop_ContainerRunning.png)

Going in to the CA2 Assignment and running `./gradlew runClient` in the terminal and all should work accordingly.

![](CA4 Images/IDE_runClient.png)

And there you go. We successfully built a chat server using a Docker Container.

![](CA4 Images/Chatter_Running.png)

And here is the confirmation that the chat server is indeed running as intended.

![](CA4 Images/DockerDesktop_serverWorking.png)

Now this is the version where we "built" the Jar file inside the Container. But what if we wanted to build the jar file in our
host machine and transfer it to the Container?

Let's see how we proceed to do that.

As you can see it's even more simple than building the Jar file inside the container. 

![](CA4 Images/VersioTwo/VScode_DockerFile.png)

Two important differences here are :

1. The `Jar` file of the Chat Server app has to be located in the same root folder as the `Dockerfile`.

2. `COPY` : Copies new files or directories from `<src>` and adds them to the filesystem of the container at the path `<dest>`.


   Example: `COPY test.txt relativeDir/` where test.txt is the `<src>` and relativeDir/ is the `<dest>`.
   
The `Jar` file that we want to copy is located in a build folder located inside the same root folder as the `Dockerfile`.

With the `Dockfile` set up just repeat the exact same steps as before.

First, run `docker build -t [docker_image_name]` in the terminal. In `docker_image_name` you can put whatever name you'd like.
In my case it was chat_server_image_versiontwo.

![](CA4 Images/VersioTwo/VScode_DockerBuild.png)

After creating the image run `docker build -d -p 59001:59001 [docker_image_name]` in the terminal and all should be working 
just like before.

![](CA4 Images/VersioTwo/VScode_DockerRun.png)

Here is the second container running with the newly created image.

![](CA4 Images/VersioTwo/DockerDesktop_Containers.png)

The second image created with the given name.

![](CA4 Images/VersioTwo/DockerDesktop_Images.png)

And finally the container running the working Chat server App.

![](CA4 Images/VersioTwo/DockerDesktop_ContainersWorking.png)

And now to finish all off we want to tag our newly created images and publish them to Docker hub.

First go to https://hub.docker.com/ and create an account for personal use.

With the account created go to the terminal and run `docker login` to be able to push your images to the docker hub.

After the login, we now want to tag our created image.

To tag the image go to the terminal and run `docker tag [docker_image_name]:[tag] [repository_name]/[docker_image_name]:[tag]`

`[docker_image_name]` : The name of the image we want to tag
 `[repository_name]` : The name of the repository holding the image.
`[tag]` : The tag we want to add to the image.

With the image tagged now just run `docker push [repository_name]/[docker_image_name]:[tag]` in the terminal, and it should create the repository
with the selected image  with the given tag.

Do this to both images created, and you should have something similar to this.

![](CA4 Images/DockerHub.png)

And this concludes this week's assignment.

## Part 1.2 : Containers with docker - Using multiple Containers.

In this week's assignment we analyse how we define and run multi-container applications. More specifically,
how do we set up our react-and-spring-data-rest Application ( the one used in CA3 ) so that one of the containers has
a `Dockerfile` that handles the frontend, and the other holds the database. 

To be able to do this we make use of Docker Compose. But what is Docker Compose?

Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure
your applications services. Then, with a single command, you create and start all the services from your configuration.

Using Compose is basically a three-step process:

 1. Define our app's environment with a `Dockerfile` so it can be reproduced anywhere.


 2. Define the services that make up our app in `docker-compose.yml` so they can be run together in an isolated environment.


 3. Run `docker-compose up` and it runs our entire application.  

To start the task at hand we were provided the code for the assignment from : https://bitbucket.org/atb/docker-compose-spring-tut-demo/src

This code is what we will be working with since it already provides the necessary `Dockerfiles` and `docker-compose.yml` file so all
that is need to be done is alter the code to use our own repository.

Cloning the code provided into our computer and opening up the `docker-compose` file should reveal something similar to this.

![](CA4 Images/Part 2/VScode_DockerCompose.png)

Now let's go through what each instruction the `docker-compose` means.

 - `services` : Here we define what containers we want to build from `Dockerfiles` contained inside folders, in this case we are going to build two containers from
two `Dockerfiles`. One in a folder called web and another in on called db. One of the advantages here is that we can already map the ports of the containers, instead of
having to do it manually in the terminal. Another thing to point out is the `depends_on` instruction. This instruction tell the web container that it depends on
the db container to be built in order to be able to be built as well, meaning that guarantees an order of container building ( db first, then web).


 - `network` : Where we specify our custom networks for the containers. More notably what Ip the containers are going to use inside the network. In this specific case
we are telling that all the containers have to be part of the subnet `192.168.33.0/24`. The Web container gets `192.168.33.10` IP and
the db container gets the `192.168.33.11` IP.

 - `volumes` : This is how we share folders between the host machine and the Docker container. Where we are stating that we want our data folder ( inside our host machine ) to also appear
inside the Docker container in the /usr/src/data-backup folder.

And this covers all the `docker-compose` has to offer.

Now here we can see the `Dockerfile` for the Web container. It's frighteningly similar to the Vagrant File Configuration we did in CA3.

The only major difference here is that instead of downloading tomcat as we did in Vagrant, we are using an image that already has tomcat built in
so it makes our job easier.

![](CA4 Images/Part 2/VScode_DockerWeb.png)

There isn't anything particularly noteworthy to stand out from the db `Dockerfile`. Again very similar to a Vagrant file configuration.

![](CA4 Images/Part 2/VScode_DockerDB.png)

With all set up and ready to go, go to the terminal and run `docker-compose build --no-cache`. the `--no-cache` option might not be 
necessary in the first time building bit it guarantees that the image is built from scratch and that there is nothing saved from previous
attempts of build. ( Think of it as clean build from gradle for example).

After the build is done just put `docker-compose up` and if all went well you should get something like this.
 
![](CA4 Images/Part 2/VScode_DockerRunning.png)

If you go to the Docker Desktop we can see that the Docker compose is in fact running successfully.

![](CA4 Images/Part 2/DockerDesktop_Containers.png)

Taking a peak inside and there you have it, both containers with their respective images running without any issue.

![](CA4 Images/Part 2/VScode_DockerRunning.png)

We're saying it's running without any issue, but we are talking about the containers. Let's check if the application is running correctly.

By using entering http://localhost:8080/basic-0.0.1-SNAPSHOT/ with the dockers running we can see that our application is indeed running as expected.
But what about the database?

![](CA4 Images/Part 2/Brower_AppRunning.png)

Going to http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console and using `jdbc:h2:tcp://192.168.33.11:9092/./jpadb` as the connection string in JDBC URL parameter, we can indeed verify that the db 
is also working as intended.

![](CA4 Images/Part 2/Browser_DatabaseRunning.png)

Now let's do the same as we did in CA3 and run the `SELECT` query to check if everything is in working order. And in fact all is working as expected.

![](CA4 Images/Part 2/Brower_Select.png)

Now, so far we haven't really used the volume feature from the `docker-compose.yml` file. Let's transfer the db file from the docker container to our data folder as sort of a backup.

To do this, open the terminal and type `docker exec -it [container_id] bash`.

What we are doing is getting inside the bash terminal of the container that we selected, so we can transfer the files from the Docker to our machine.

Once inside the container just type `cp /usr/src/app/jpadb.mv.db /usr/src/data-backup` and the database file will be copied into the data-backup folder, which consequently goes to our data folder in
the host machine because of the way we configured the volume as previously mentioned.

![](CA4 Images/Part 2/VScode_DockerExec.png)

If all done correctly you should get something like this.

![](CA4 Images/Part 2/VScode_jpaDbCopy.png)

And this concludes this week assignment.



# 2. Analysis of the Alternative

The alternative to Docker that we will be analysing will be Kubernetes.

This is going to be a little different from previous alternative implementation since we are going to analyse 
the alternative and not implement it in any way.

So what is **Kubernetes** exactly?

Kubernetes is an open-source container orchestration platform for managing, automating, and scaling containerized
applications. Although Docker Swarm is also an orchestration tool, Kubernetes is the defacto standard for container
orchestration because of it's greater flexibility and capacity to scale.

With **Kubernetes** you can:

- Orchestrate containers across multiple hosts.
- Make better use of the hardware to maximize resources needed to run your enterprise apps.
- Control and automate application deployments and updates.
- Mount and add storage to run stateful apps.
- Scale containerized applications and their resources on the fly
- Declaratively manage services, which guarantees the deployed applications are always running the way you intended them to run.
- Health-check and  self-heal your apps with autoplacement, autorestart, autoreplication, and autoscaling.

There are some commom terms used in Kubernetes that are the base of the software and it's important to know them and break them down.

**Control plane**: The collection of processes that control Kubernetes nodes. This is where all task assignments originate.

**Nodes** : These machines peform the requested tasks assigned by the control plane.

**Pod** : A group of one or more containers deployed to a single node. All containers in a pod share an IP address, IPC, hostname, and other resources.

**Replication Controller** : This controls how many identical copies of a pod should be running somewhere on the cluster.

**Service**: This decouples work definitions from the pods. **Kubernetes** service proxies automatically get service requests to the right pod.

**Kubelet** : This service runs on nodes, reads the container manifests, and ensures the defined containers are started and running.

**kubectl** : The command line configuration tool for Kubernetes.

